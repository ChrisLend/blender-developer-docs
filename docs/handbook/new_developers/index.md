# New Developer Introduction

## Steps

Guide for new developers looking to get involved in Blender development.

* [Set up](setup.md) a Blender build and development environment
* [Get in contact](contact.md) with other developers and contributors
* [Pick a project](pick_a_project.md) to work on
* Learn to [navigate the code](navigate_code.md)
* [Get tips](tips.md) to become efficient
* See [frequently asked questions](faq.md) about Blender development.

## Other Ways to Help

Even if you don't write code, there are other ways to help.

* [Write Documentation](https://docs.blender.org/manual/en/latest/contribute/index.html)
* [Triage Bugs](../bug_reports/help_triaging_bugs.md)
* [Translate Blender](../translating/translator_guide.md)
* [Feedback and Testing](https://devtalk.blender.org/c/feature-design-feedback/30)
* [Modules Membership](../organization/modules.md)
