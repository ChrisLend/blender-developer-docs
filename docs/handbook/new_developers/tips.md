# Tips for New Developers

## Things to Learn

Here are things you'll probably want to learn as you get into
development.

- Learn to search the source code \*efficiently\*
  (if you see a word in the interface, chances are searching for it will
  take you to the code related to it, or close enough).
- Learn basic regex (many search tools & IDE's support them), it looks
  confusing but you can find some cheat sheets online to help.
- Learn to use a debugger to at minimum check the file and line number
  of a crash (view a stack trace).
- Use git to make temporary branches and stashing changes.
  *Allow for some time to become comfortable using git, initially simply
  updating is enough to get started, but eventually you will want to use
  some of its more advanced features.*
- Learn how to make pull requests (to share your changes with others).
- Use `git blame` to find who changed some line of code, when and why.
- Rather than guessing why something is slow, use a profiler - there are
  many profilers around and from my experience they all have trade-offs
  between setup time, execution speed and useful output.
- If you're debugging memory related errors on Linux.
  Try
  [Address-Sanitizer](../tooling/asan.md)
  or
  [Valgrind](../tooling/valgrind.md),
  they're very useful tools and in some cases can save hours of tedious
  troubleshooting.

## Efficiency

- Disabling features when building Blender (OpenVDB, Collada, FFMpeg,
  Cycles, LibMV... etc) can speed up rebuilds considerably.
  *Unless you intend to develop on any of these areas of course.*

- While you will eventually want to use a debugger to step over the
  source try using `printf()` for debugging, sometimes if you're not sure of
  how something works it's good to add prints into the code, for your
  own code or even if you read it and want to understand it better, this
  is a very simple/stupid way to check on things but handy, fast
  rebuilds make this less of a hassle.
