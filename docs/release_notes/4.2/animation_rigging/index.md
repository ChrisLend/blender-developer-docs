# Blender 4.2: Animation & Rigging

## Motion Paths
- Motion Path points now use the Vertex theme color (blender/blender@fc025abde6).
- You can now specify a custom color for anything before or after the current frame. Previously this was a single color (blender/blender@883af6dd63).

## Graph Editor
- Modified the way the Ease operator works. It now creates an S-Curve shape that can be moved to
  favor either the left or the right end. In addition to that, a "Sharpness" property was added to
  control how abrupt the change is (blender/blender@62f140e048,
  [Manual](https://docs.blender.org/manual/en/4.2/editors/graph_editor/fcurves/editing.html#ease)).
- Performance when moving keys in scenes with a lot of keys has been improved. (blender/blender@8a79212031, blender/blender@6a55a126e9, blender/blender@3eda9619f9, blender/blender@0157fede80)

## Dope Sheet

![Keys of the new 'Generated' type, between regular keys. Some of the 'generated' keys are selected, to show the color difference.](key_type_generated_dark_theme.webp)

- New key type: 'Generated' (blender/blender@51e1f29a68). This new key type indicates that the key
  was set by some automated tool (like an add-on), rather than manually by an animator. It is drawn
  smaller & dimmed in the Dope Sheet. These keys can still be operated on like any other, including
  changing their key type to something else.

## NLA
- Performance of the editor has been improved when only viewing a section of a long action
  (blender/blender@24b4461be3).
- Update NLA "Make Singe User" operator modal text.
 (blender/blender@298c9276e9)

## Armatures
- Subdividing a bone names the new bones in sequential order (blender/blender@9f0cc7fe64).
  Subdividing "Bone" makes "Bone.001" its first child, "Bone.002" the next, etc. Previously this
  would have been "Bone", "Bone.003", "Bone.002", "Bone.001".

## Constraints
- The behavior of the "Limit Rotation" constraint has been modified to fix a bug ([Bug Report](https://projects.blender.org/blender/blender/issues/117927), blender/blender@ed2408400d). In older versions the constrained object would flip back to the minimum when an angle is higher than 180º. This is now changed so values higher than 180º are supported. This means a minimum of 90º and a maximum of 270º now works properly. Also instead of snapping back to the minimum, the rotation will snap to the closest boundary when the rotation is out of the allowed range. Rigs relying on the old behavior of snapping back to the minimum might not work as expected.

## Copy Global Transform add-on

Copy Global Transform ([manual][copy-global-transform-doc]) has gotten two new, related features
(blender/blender-addons@0b1d563a2f4f04477229d77944581c205671813f):

[copy-global-transform-doc]: https://docs.blender.org/manual/en/latest/addons/animation/copy_global_transform.html

![Copy Global Transform: Fix to Camera and Relative Copy-Paste](copy_global_transform_panel.webp)

### Fix to Camera

Also known as "bake to camera", this operator will ensure selected objects/bones remain static
(relative to the camera) on unkeyed frames.

This is done by generating new keys. These keys will be of type 'Generated' so that it remains clear
which keys were manually created, and which were generated. This way the tool can be re-run to
re-generate the keys.

It operates on the scene frame range, or on the preview range if that is active.

### Relative Copy-Paste

The "Relative" panel has copy/paste buttons that work relative to some object. That object can also
be chosen in the panel. If no object is chosen, the copy/paste will happen relative to the active
scene camera.

## Python API
- `rna_struct.keyframe_insert()` now has a new, optional keyword argument `keytype` to set the key type
  (blender/blender@1315991ecbd179bbff8fd08469dda61ec830d72f). This makes it possible to set the new
  key's type directly. Example: `bpy.context.object.keyframe_insert("location", keytype='JITTER')`.
- There is a new function on the F-Curve: `bake`. This creates keys at a given interval for a given range on the existing curve
  (blender/blender@20a4c9c928).
