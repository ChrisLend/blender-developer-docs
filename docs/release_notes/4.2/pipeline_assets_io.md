# Blender 4.2: Pipeline, Assets & I/O

## Collection Exporters
File format exporters can now be associated with Collections. One or
more exporters can be added and configured in the Collection properties
panel. (blender/blender@509a7870c3570cbf8496bcee0c94cdc1e9e41df5)

This feature streamlines the process of re-exporting the same asset(s)
repeatedly, making it more convenient. Additionally, it enables
simultaneous export to multiple formats from a central location.

It also provides a central place to consolidate export settings which
are stored inside the .blend file for easy sharing and persistence
across Blender sessions.

## USD
- The new Hair `Curves` object type is now supported for both Import and Export
<!--
- Point cloud import ...
- Other notable improvements ...
-->

## Alembic
- The new Hair `Curves` object type is now supported for both Import and Export
- Addressed long-standing issue where animated curves would not update during render
