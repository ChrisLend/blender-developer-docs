# Blender 4.2: Rendering

## Color Management

### Khronos PBR Neutral

The Khronos PBR Neutral view transform has been added. (blender/blender@d1cbb10d17)

This tone mapper was designed specifically for PBR color accuracy, to get sRGB
colors in the output render that match as faithfully as possible the input sRGB
base color in materials, under gray-scale lighting. This is aimed toward product
photography use cases, where the scene is well-exposed and HDR color values are
mostly restricted to small specular highlights.

## Other

- Motion Blur settings are now shared between Cycles and EEVEE. (blender/blender@74b8f99b43)
