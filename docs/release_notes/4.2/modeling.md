# Blender 4.2: Modeling & UV

## UV Editing

### Transform

- The Edge and Vert Slide operators have been ported to the UV Editor
  (blender/blender@aaadb5005e).
- The `Set Snap Base` feature (key `B` ) have been enabled in the UV Editor
  (blender/blender@a56a975760).
- The 'Absolute Grid Snap' option has been replaced with a new snap mode: Snap to Grid.
  (blender/blender@f0479e915f).

## Curves Edit Mode

- Curves edit mode draws evaluated curves and handles now (blender/blender@15dbfe54e46).
- Support converting curves types (blender/blender@23265a2b6d16, blender/blender@3f2c4db95105).
- New *Add* menu to insert some primitive curves (blender/blender@549d02637af6).
- New operator to set the handle type for bezier curves (blender/blender@549d02637af6).
- New operator to subdivide curves (blender/blender@548df007a59a).
- New operator to switch direction of curves (blender/blender@6c25c66194d7).
- New operator to toggle whether curves are cyclic (blender/blender@d315a6a793d0).
