# Blender 2.80: User Interface API

## Toolbar

The 3D viewport toolbar was replaced with a new toolbar that only
contains active tools. Addons that were previously placed in the old
toolbar can be updated in a few different ways:

- Addons that need a more persistent user interface can register tabs
  and panels in the sidebar of editors, typically the 3D viewport. This
  moves the addon user interface from one side of the 3D viewport to the
  other.
- Interactive tool addons can be converted to the new active tools
  system, and be placed in the toolbar. For example addons for drawing
  or measuring, interactive mesh modeling tools, or interactive
  procedural geometry generators. By adding gizmos in the 3D viewport
  these can be made more intuitive.
- Command like addons can be added in menus, with any settings in the
  adjust operation popup. This can be in the editor header menus, or in
  the context menu.
- Settings specific to a mode can be added as tool settings panel, which
  appear in the top bar and the properties editor tool settings tab.

## Buttons and Panels

#### Single Column Layout

Most of the Blender user interface has been changed to use a single
column layout for properties, particularly the properties editor. For
consistency with the rest of the Blender UI addons should follow the
same layout.

``` Python
def draw(self, context):
    layout = self.layout
    layout.use_property_split = True # Active single-column layout

    scene = context.scene
    view = scene.view_settings

    layout.prop(view, "view_transform")
    layout.prop(view, "look")
```

#### Sub Panels

Panels can now have sub panels to organize their contents and hide
advanced or rarely used functionality by default. Sub panels are created
by adding a `bl_parent_id` referring to the parent panel.

``` Python
# Parent panel
class RENDER_PT_color_management(RenderButtonsPanel, Panel):
    bl_label = "Color Management"
    ...

# Sub panel
class RENDER_PT_color_management_curves(RenderButtonsPanel, Panel):
    bl_label = "Use Curves"
    bl_parent_id = "RENDER_PT_color_management"
    ...
```

## Icons

The icon set has been refreshed, with icons added, removed or updated to
reflect changes in Blender functionality. The table below should make it
easier to update Python scripts with broken icon identifiers.

The Icon Viewer addon built into Blender can be used to get an overview
of all the new icons.

#### Renamed

    Old                 New

    ZOOMIN              ADD
    ZOOMOUT             REMOVE
    NEW                 FILE_NEW
    BBOX                SHADING_BBOX
    POTATO              SHADING_TEXTURE
    SMOOTH              SHADING_RENDERED
    SOLID               SHADING_SOLID
    WIRE                SHADING_WIRE
    ORTHO               XRAY
    BUTS                PROPERTIES
    IMAGE_COL           IMAGE
    OOPS                OUTLINER
    IPO                 GRAPH
    SCRIPTWIN           PREFERENCES
    CURSOR              PIVOT_CURSOR
    ROTATECOLLECTION    PIVOT_INDIVIDUAL
    ROTATECENTER        PIVOT_MEDIAN
    ROTACTIVE           PIVOT_ACTIVE
    FULLSCREEN          WINDOW
    LAMP                LIGHT
    LAMP_DATA           LIGHT_DATA
    OUTLINER_OB_LAMP    OUTLINER_OB_LIGHT
    OUTLINER_DATA_LAMP  OUTLINER_DATA_LIGHT
    LAMP_POINT          LIGHT_POINT
    LAMP_SUN            LIGHT_SUN
    LAMP_SPOT           LIGHT_SPOT
    LAMP_HEMI           LIGHT_HEMI
    LAMP_AREA           LIGHT_AREA
    VISIBLE_IPO_ON      HIDE_OFF
    VISIBLE_IPO_OFF     HIDE_ON

#### Removed

    LINK_AREA           (use LINKED)
    PLUG                (use PLUGIN)
    EDIT               
    GAME               
    RADIO              
    DOTSUP             
    DOTSDOWN            
    LINK                (maybe use DOT, LAYER_ACTIVE or LAYER_USED)
    INLINK             
    GO_LEFT            
    TEMPERATURE        
    SNAP_SURFACE       
    MANIPUL            
    BORDER_LASSO       
    MAN_TRANS          
    MAN_ROT            
    MAN_SCALE          
    BBOX               
    RENDER_REGION      
    RECOVER_AUTO       
    SAVE_COPY          
    OPEN_RECENT   
    LOAD_FACTORY
    ALIGN
    SPACE2
    ROTATE
    SAVE_AS
    BORDER_RECT
    ROTACTIVE
    ORTHO
    OOPS

#### Added

    ALEMBIC
    ALIGN_BOTTOM
    ALIGN_CENTER
    ALIGN_FLUSH
    ALIGN_JUSTIFY
    ALIGN_LEFT
    ALIGN_MIDDLE
    ALIGN_RIGHT
    ALIGN_TOP
    ASSET_MANAGER
    BOLD
    DECORATE
    DECORATE_ANIMATE
    DECORATE_DRIVER
    DECORATE_KEYFRAME
    DECORATE_LIBRARY_OVERRIDE
    DECORATE_LINKED
    DECORATE_LOCKED
    DECORATE_OVERRIDE
    DECORATE_UNLOCKED
    DRIVER_DISTANCE
    DRIVER_ROTATIONAL_DIFFERENCE
    DRIVER_TRANSFORM
    DUPLICATE
    FACE_MAPS
    FAKE_USER_OFF
    FAKE_USER_ON
    GP_MULTIFRAME_EDITING
    GP_ONLY_DSELECTED
    GP_SELECT_POINTS
    GP_SELECT_STROKES
    GREASEPENCIL
    HEART
    ITALIC
    LIBRARY_DATA_OVERRIDE
    LIGHTPROBE_CUBEMAP
    LIGHTPROBE_GRID
    LIGHTPROBE_PLANAR
    LINE_DATA
    MATCLOTH
    MATFLUID
    MATSHADERBALL
    MOD OPACITY
    MOD_HUE_SATURATION
    MOD_INSTANCE
    MOD_NOISE
    MOD_OFFSET
    MOD_PARTICLE_INSTANCE
    MOD_SIMPLIFY
    MOD_THICKNESS
    MOD_TIME
    MODIFIER_OFF
    MODIFIER_ON
    MOUSE_LMB
    MOUSE_LMB_DRAG
    MOUSE_MMB
    MOUSE_MMB_DRAG
    MOUSE_MOVE
    MOUSE_RMB
    MOUSE_RMB_DRAG
    NORMALS_FACE
    NORMALS_VERTEX
    NORMALS_VERTEX_FACE
    OBJECT_ORIGIN
    ONIONSKIN_OFF
    ONIONSKIN_ON
    ORIENTATION_GIMBAL
    ORIENTATION_GLOBAL
    ORIENTATION_LOCAL
    ORIENTATION_NORMAL
    ORIENTATION_VIEW
    OUTLINER_DATA_GREASEPENCIL
    OUTLINER_OB_IMAGE
    OUTLINER_OB_LIGHTPROBE
    OVERLAY
    PRESET
    PRESET_NEW
    SEALED
    SETTINGS
    SHADERFX
    SMALL_CAPS
    SYSTEM
    THREE_DOTS
    TOOL_SETTINGS
    TRACKING
