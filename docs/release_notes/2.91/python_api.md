# Blender 2.91: Python API

## Compatibility

- The Principled BSDF shader has a new Emission Strength input. Material
  importers and exporters need to be updated to take this into account.
  (blender/blender@b248ec9)

## Scene Ray Cast

The first parameter to the `scene.ray_cast()` function has changed
(blender/blender@e03d53874dac5f,
[A82ed41ec63](https://developer.blender.org/rBA82ed41ec632483fa9260d90dae7afdf3192c509b)).
It now receives a Depsgraph instead of a View Layer. This made it
possible to [fix a crash](https://developer.blender.org/T79127), and
also fixes potential issues when ray-casting is done while rendering
(for example from a custom driver function).

``` Python
# Old call example:
context.scene.ray_cast(context.view_layer, ...)

# New call example:
context.scene.ray_cast(context.view_layer.depsgraph, ...)
```

The other parameters and the return value has remained the same.

## Other changes

- Add `ghost` argument to `Struct.is_property_set`, so it's possible
  to test if an operator's property is being reused from the last time
  it was called
  (blender/blender@d3bcbe10c20e8b).
- `wm.read_homefile` now has a `use_factory_startup` argument, which
  can be used as a faster alternative when resetting the file contents
  for tests since `wm.read_factory_settings` resets preferences
  (re-registering all add-ons)
  (blender/blender@cf67ba848f2f42e0b7320aa0654ba89bd14a1416).
- Add `UILayout.introspect()` for scripts to access the UI layout (for
  testing/debugging)
  (blender/blender@428a1aaf7372aaad793fe7cc03128db18e3ae602).
- New `Mesh.attributes` API for accessing custom geometry attributes
  on meshes. This replaces `vertex_layers_float`,
  `vertex_layers_int`, `vertex_layers_string`,
  `polygon_layers_float`, `polygon_layers_int` and
  `polygon_layers_string`, which are now deprecated. Instead
  `.attributes` provides a single list of geometry attributes for all
  combinations of geometry element and data types. Further, the data
  types have been extended with 3D Float Vector, Byte Color and Float
  Color.
  (blender/blender@565510b)
- New `VolumeGrids.save()` function to save volume grids and metadata
  to disk.
  (blender/blender@fc76750)
- Custom property names can no longer contain `"`, `'`, or `\`.
  These characters can cause problems with drivers and Python and may
  not have worked properly in the past.
  (blender/blender@cbae82ba960a)
- Actions are bound to the type of the datablock they're assigned to; a
  Camera Data action cannot be assigned to an Object. This "locking" used
  to happen when the animation was evaluated; now it happens when the
  Action is assigned.
  (blender/blender@64583f3e8d0f)
- `sys.executable` now points to the Python interpreter (instead of
  the Blender executable)
  (blender/blender@04c5471ceefb41c9e49bf7c86f07e9e7b8426bb3).
  This resolves `multiprocessing` which failed to spawn new processes
  on WIN32.
- `bpy.app.binary_path_python` has been deprecated (use
  `sys.executable` instead).
- Since porting `bpy.ops.uv.smart_project` to C
  (blender/blender@850234c1b10a),
  `angle_limit` has to be given in radians.
