# Schema Versions

The specification for the different schema versions.

| Version           | Blender Version Initial                      | Blender Version Final                        |
| ----------------- | -------------------------------------------- | -------------------------------------------- |
| [1.0.0](1.0.0.md) | [4.2.0](../../../release_notes/4.2/index.md) | - |