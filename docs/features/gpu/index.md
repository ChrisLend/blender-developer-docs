# GPU/Viewport Module

The GPU/Viewport Module main responsibility is 
drawing pixels to the screen based on common graphics libraries (GL) provided
by operating systems. OpenGL, Metal and Vulkan are different GLs we integrate.
The integration is developed via an abstraction layer called the gpu-module.

Low level APIs for UI code. The UI/UX module uses these low level APIs for
drawing the user interface to the screen. These low level APIs include:

* Immediate mode emulation
* Icon drawing
* Font drawing
* Widget drawing

Drawing viewport require a different approach compared to regular screens in
order to be able to optimize the performance of the 3d viewport. We call name
this the draw manager. The draw manager is used for drawing

* EEVEE
* Workbench
* Overlays in 3D Viewport, but also in image editor and sequencer.
* Image editors
* Grease pencil 