# Span

A `blender::Span<T>` ([source](https://projects.blender.org/blender/blender/src/branch/main/source/blender/blenlib/BLI_span.hh)) references an array that is owned by someone else. It is just a pointer and a size. Since it is so small, it should generally be passed by value.

`Span` is the main way to pass multiple elements into a function and should be prefered over e.g. `const Vector &` because it gives the caller more flexibility.

The memory directly referenced by the span is considered to be `const`. This is different from `std::span` where constness is not the default. When non-constness is required, a `MutableSpan` can be used. This also makes the intention more clear.
