# Strings

Blender usually stores strings as `std::string`. If strings are passed around without transfering ownership, `blender::StringRef` ([source](https://projects.blender.org/blender/blender/src/branch/main/source/blender/blenlib/BLI_string_ref.hh)) should be used. `StringRef` is a non-owning slice of a string. It's just a pointer and a size and should generally be passed around by value. If a string with null-termination is required, `StringRefNull` should be used instead.
